function radiobuttons(checked)
{
    switch(checked)
    {
        case '1': document.getElementById('txtNewArrivals').checked=true;
        document.getElementById('txtNew').checked=false;
        document.getElementById('txtBestSeller').checked=false;
        document.getElementById('none').checked=false;
        break;
        case '2': document.getElementById('txtNew').checked=true;
        document.getElementById('txtNewArrivals').checked=false;
        document.getElementById('txtBestSeller').checked=false;
        document.getElementById('none').checked=false;
        break;
        case '3': document.getElementById('txtBestSeller').checked=true;
        document.getElementById('txtNew').checked=false;
        document.getElementById('txtNewArrivals').checked=false;
        document.getElementById('none').checked=false;
        break;
        case '4': document.getElementById('none').checked=true;
        document.getElementById('txtNew').checked=false;
        document.getElementById('txtNewArrivals').checked=false;
        document.getElementById('txtBestSeller').checked=false;
        break;
        default: alert(checked); 
        break;
    }
}

function CheckID() //Check if ID is NULL
{
    var x = document.getElementById('txtMa').value;
    if (x == "")
    {
        document.getElementById('errorMa').innerHTML = "Vui lòng nhập mã sản phẩm"
    }else document.getElementById('errorMa').innerHTML = ""
}

function CheckName()//Check if Name is NULL
{
    var x = document.getElementById('txtName').value;
    if (x == "")
    {
        document.getElementById('errorName').innerHTML = "Vui lòng nhập tên sản phẩm"
    }else document.getElementById('errorName').innerHTML = ""
}

function CheckPrice()//Check if Unit_price is NULL or Is not a number
{
    var x = document.getElementById('txtPrice').value;
    if (x == "")
    {
        document.getElementById('errorPrice').innerHTML = "Vui lòng nhập giá sản phẩm"
    }
    else
    {
        if (isNaN(document.getElementById('txtPrice').value)==true)
        {
            document.getElementById('errorPrice').innerHTML = "Giá sản phẩm không hợp lệ"
        }
        else document.getElementById('errorPrice').innerHTML = ""
    }
}