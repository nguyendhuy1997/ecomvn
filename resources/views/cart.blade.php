@extends('layout.layout') 
@section('title', 'Cart') 
@section('content')
<!-- checkout page -->


    <div class="privacy">
        <div class="container">
            <!-- tittle heading -->
            <h3 class="tittle-w3l">Checkout
                <span class="heading-style">
                        <i></i>
                        <i></i>
                        <i></i>
                    </span>
            </h3>
            <!-- //tittle heading -->
            <div class="checkout-right">
                <h4>Your shopping cart contains:
                    <span><?php if(isset($_SESSION['cart'])){ echo count($_SESSION['cart']); }else echo 0;?></span> Products
                </h4>
                <div class="table-responsive">
                    <table class="timetable_sub">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Product Name</th>
                                <th>Price</th>
                                <th>Subtotal</th>
                                <th>Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                          
                                
                         <?php if(isset($_SESSION['cart'])){ ?>
                            <?php foreach ($_SESSION['cart'] as $p) {
                            
                         ?>
                            <tr class='<?php echo ( 'rem3'.' product-'.$p[ 'id']) ?>' >
                                <td class="invert-image">
                                    <a href="#">
                                            <img src="<?= $p['img']; ?>" alt=" " class="img-responsive">
                                        </a>
                                </td>
                                <td class="invert">
                                    <div class="quantity">
                                        <div class="quantity-select">
                                            <div class="entry value-minus " data-type="minus" data-id=<?php echo $p[ 'id']; ?>>&nbsp;</div>
                                            <div class="entry value">
                                                <span data-id=<?php echo $p[ 'id'] ?> id=<?php echo $p['id'] ?> > <?= $p['quantity']; ?></span>
                                            </div>
                                            <div class="entry value-plus active" data-type="plus" data-id=<?php echo $p[ 'id']; ?>>&nbsp;</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="invert">
                                    <?= $p['name']; ?>
                                </td>
                                <td class="invert" id="price-<?= $p['id'];?>" data-price=<?=$p[ 'price']?> >
                                    <?= number_format($p['price']); ?>đ</td>
                                <td  class="invert cls-subtotal" data-subtotal=<?=$p[ 'price']*$p[ 'quantity'];?> id="subtotal-<?= $p['id'];?>">
                                    <span><?= number_format($p['price']*$p['quantity']); ?></span> đ</td>
                                <td class="invert">
                                    <div class="rem">
                                        <div class="close3" data-delete=<?php echo $p[ 'id']; ?>> </div>
                                    </div>
                                </td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td id="total">Total: <span><?php $total=0;
                        foreach($_SESSION['cart'] as $p)
                        {
                            $total+=$p['price']*$p['quantity'];
                        } 
                            echo number_format($total); ?></span> đ</td>
                                <td id="total-value" data-totalValue=<?=$total ?>></td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="checkout-left">
                <div class="address_form_agile">
                    <h4>Bill Information</h4>
                    <form action="{{route('bill')}}" method="post" class="creditly-card-form agileinfo_form">
                            @csrf
                        <div class="creditly-wrapper wthree, w3_agileits_wrapper">
                            <div class="information-wrapper">
                                <div class="first-row">
                                    <div class="controls">
                                        <input class="billing-address-name" type="text" name="name" placeholder="Full Name" required="">
                                    </div>
                                    <div class="w3_agileits_card_number_grids">
                                        <div class="w3_agileits_card_number_grid_left">
                                            <div class="controls">
                                                <input type="number" placeholder="Mobile Number" name="phone" required="">
                                            </div>
                                        </div>
                                        <div class="w3_agileits_card_number_grid_right">
                                            <div class="controls">
                                                <input type="text" placeholder="Address" name="address" required="">
                                            </div>
                                        </div>
                                        <div class="clear"> </div>
                                    </div>
                                    {{-- <div class="controls">
                                        <input type="text" placeholder="Town/City" name="city" required="">
                                    </div>
                                    <div class="controls">
                                        <select class="option-w3ls">
                                                <option>Select Address type</option>
                                                <option>Office</option>
                                                <option>Home</option>
                                                <option>Commercial</option>
    
                                            </select>
                                    </div> --}}
                                </div>
                                <button class="submit check_out" type="submit">Delivery to this Address</button>
                            </div>
                        </div>
                    </form>
                    {{-- <div class="checkout-right-basket">
                        <a href="payment.html">Make a Payment
                                <span class="fa fa-hand-o-right" aria-hidden="true"></span>
                            </a>
                    </div> --}}
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- //checkout page -->
    <!-- update cart -->
    <script>
        $('.value-minus, .value-plus').on('click',function(){
            var id=$(this).data('id');
            var type=$(this).data('type');
            $.ajax({
				type:'get',
				url:'{{route('updatecart')}}',
				data:{id_products:id,type:type},
				success:function(data){
                if(data!=0)
                {
                   $('#' + id).html(data);
                   var price =$('#price-'+id).data('price');
                   var newSubTotal = price * data;
                   var total = $('#total-value').data('totalvalue');
                   $('#subtotal-' + id +' span').html(formatNumber(newSubTotal));
                   if(type === 'minus'){
                        total = total - price;
                   }else total = total + price;
                   $('#total-value').data('totalvalue', total);
                   $('#total span').html(formatNumber(total));
                }
				}
            	});
        });
        function formatNumber (num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
            }
    </script>
    <!-- //update cart -->

    <!-- delete cart -->
    <script>
        $('.close3').on('click',function(){
            var total = parseInt($('#total span').html().replace(/,/g, ''));
            var id=$(this).data('delete');
            var count=parseInt($('.checkout-right h4 span').html().replace(/,/g, '')); 
            $.ajax({
				type:'get',
				url:'{{route('deletecart')}}',
				data:{id_products:id},
				success:function(data){
                    var subtotal = parseInt($('#subtotal-'+id+' span').html().replace(/,/g, ''));
                    $('#total span').html(formatNumber(total-subtotal));
                    $('.checkout-right h4 span').html(count-1);
                    $('.product-' + id).empty();
				}
            	});
          
        });
        function formatNumber (num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
            }
    </script>
    <!-- //delete cart -->
@endsection