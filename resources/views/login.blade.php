<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html lang="zxx">

<head>
	<title>Validify Login Form Flat Responsive Widget Template :: w3layouts</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<meta name="keywords" content="Validify Login Form Responsive Widget,Login form widgets, Sign up Web forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements"
	/>
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- Meta tag Keywords -->
	<!-- css files -->
	<link rel="stylesheet" href="./css/login.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link rel="stylesheet" href="./css/login-font.css">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->
	<!-- web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Nova+Round" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
	<!-- //web-fonts -->
</head>

<body>
	<!-- title -->
	<h1>
		<span>Login Form</span>
	</h1>
	<!-- //title -->
	<!-- content -->
	<div class="sub-main-w3">
		<form id="demo" novalidate action="login" method="post">
		<!-- @csrf -->
		<input type="hidden" name ="_token" value="{{csrf_token()}}"/>
			<div class="form-group">
				<input type="email" class="form-control textbox" name="txtEmail" placeholder="E-mail" required="">
			</div>
			<div class="form-group">
				<input type="password" class="form-control textbox" name="txtPassword" placeholder="Password" required="">
			</div>
			<div class="form-group-2">
				<button class="btn btn-default btn-osx btn-lg"  type="submit">
					Login
				</button>
			
			</div>
	
			<div class="alert alert-success hidden" role="alert">You Have Successfully Logged In</div>
		</form>
		<!-- //switch -->
	</div>
	<!-- //content -->
	@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
	@endif
	<!-- copyright -->
	<div class="footer">
	<p style="font-size:20px">&copy; 
			<a size="6" href="{{route('homepage')}}">Homepage</a>
		</p>
	</div>
	<!-- //copyright -->

	<!-- Jquery -->
	<script src="js/jquery-2.2.3.min.js"></script>
	<!-- //Jquery -->
	<!-- validify plugin -->
	<script src="js/validify.js"></script>
	<script>
		$("#demo").validify({
			onSubmit: function (e, $this) {
				$this.find('.alert').removeClass('hidden')
			},
			onFormSuccess: function (form) {
				console.log("Form is valid now!")
			},
			onFormFail: function (form) {
				console.log("Form is not valid :(")
			}
		});
		$("#demo").validify({
			errorStyle: "validifyError",
			successStyle: "validifySuccess",
			emailFieldName: "email",
			emailCheck: true,
			requiredAttr: "required",
		});
	</script>
	<!-- //validify plugin -->



</body>

</html>