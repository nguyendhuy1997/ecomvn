@extends('admin.admin')
@section('content')
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-offset-2 col-md-5">
               
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2">
                <form method="post" action="" id="insert-form" >
                    @csrf
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                 <table class="login-form">
                    {{-- <tr>
                        <th>Id Product</th>
                        <th><input class="form-control" name="txtId" id="txtMa" placeholder="Mã Sản Phẩm" onblur="CheckID()" /></th>
                        <th><p id="errorMa"></p></th>
                    </tr> --}}
                    
                    <tr>
                        <th>Id Category</th>
                        <th>                    
                            <select id="txtCategory">
                                <?php foreach ($categories as $key => $value) { ?>
                                    
                                    <option value=<?= $value->id_category?>><?php echo $value->name ?> </option>
                                <?php } ?>
                            </select>
                        </th>
                        <th><p id="errorCategory"></p></th>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th><input class="form-control" name="txtName" id="txtName" placeholder="Tên Sản Phẩm"  required/></th>
                        <th><p id="errorName"></p></th>
                    </tr>
                    <tr>
                        <th>Quantity</th>
                        <th><input class="form-control" name="txtQuantity" type="number" id="txtQuantity" placeholder="Quantity" required /></th>
                        <th><p id="errorQuantity"></p></th>
                    </tr>
                    <tr>
                        <th>Price</th>
                        <th><input class="form-control" name="txtPrice" id="txtPrice" type="number" placeholder="Giá" required /></th>
                        <th><p id="errorPrice"></p></th>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <th><textarea class="form-control" rows="3" id="txtDesc" name="txtDes" placeholder="Mô Tả" required></textarea></th>
                    </tr>
                    <tr>
                        <th>Image</th>
                        <th><input class="form-control"  name="txtImage" id="txtImage" placeholder="Image" required/></th>
                    </tr>
                    <tr>
                        <th><img id="add-product-img" width="100px" src="" alt=""></th>
                    </tr>
                    {{-- <tr>
                        <th></th>
                        <th>
                            <input type="radio" id="txtNewArrivals" name="txtNewArrivals" value="1" onclick="radiobuttons('1')"> New Arrivals
                            <input type="radio" id="txtNew" name="txtNew" value="1" onclick="radiobuttons('2')"> New<br>
                            <input type="radio" id="txtBestSeller" name="txtBestSeller" value="1" onclick="radiobuttons('3')"> Best Seller
                            <input type="radio" id="none" name="none" value="0" style="margin-left: 13px;" onclick="radiobuttons('4')"> None
                        </th>
                    </tr> --}}
                 </table>
                            
                        <button type="submit" class="btn btn-default" id="btn-insert" align-center>Insert</button>
                        <button type="reset" class="btn btn-default">Refresh</button>
                        
                    </div>
                </form>
            </div>
        </div>
    </section>
</section>
<script>
    $('#txtImage').change(function(){
		$('#add-product-img').attr('src', $(this).val());
	});
    $('#insert-form').submit(function(e){
        e.preventDefault();
        var txtName = $('#txtName').val();
        var txtQuantity = $('#txtQuantity').val();
        var txtPrice = $('#txtPrice').val();
        var txtDesc = $('#txtDesc').val();
        var txtImage = $('#txtImage').val();     
        var txtCategory = $('#txtCategory').val();
        var data = {
            txtName: txtName,
            txtQuantity: txtQuantity,
            txtPrice: txtPrice,
            txtDesc: txtDesc,
            txtImage: txtImage,
            txtCategory: txtCategory,
        }
        $.ajax({
            type:'post',
            url:'{{route('addproduct')}}',
            data: data,
            dataType: 'json',
            success:function(data){
                console.log(data);
                if(data){
                    alert("Đã thêm thành công.");
                    $('#insert-form')[0].reset();
                    $('#add-product-img').attr('src', '');
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                console.log(XMLHttpRequest.responseText); 
            }    
        });
    })
</script>
@endsection