@extends('admin.admin')
@section('content')
<section id="main-content">
   <section class="wrapper">
       <div class="row">
           <table id="table-detail" class="table table-bordered">
               <tr class="success">
                   <th>ID</th>
                   <th>Full Name</th>
                   <th>Email</th>
                   <th>Role</th>
                   <th>Action</th>
                   <th>Action</th>

               </tr>
               @foreach($user as $us)
               <tr>
                   <th>{{$us->id}}</th>
                   <th>{{$us->name}}</th>
                   <th>{{$us->email}}</th>
                   <th>{{$us->role}}</th>  
                   <th><a href="{{route('admin/edituser',$us->id)}}">Edit</a></th>
                   <th><a href="{{route('admin/deleteuser',$us->id)}}">Delete</a></th>
               </tr>
               @endforeach
            </table>
           
       </div>
   </section>
</section>

<!--<script>
   $(document).ready(function(){
       $("#table-detail").colResizable({liveDrag:true});
   }
</script>-->
@endsection