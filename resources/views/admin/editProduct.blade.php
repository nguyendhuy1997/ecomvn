@extends('admin.admin')
@section('content')
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-offset-2 col-md-5">
                {{-- @if (count($errors) > 0 )
                    <div class="alert alert-danger">
                        <strong>Xảy ra lỗi!</strong>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                    </div>
                @endif --}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2">
                <form method="post" id="submit-form" action="{!! route('admin/posteditproduct') !!}"  enctype="multipart/form-data" >
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <table>
                        </tr>
                            <th>Id Product </th>
                            <th><input class="form-control" value="{{$product->id_products}}" id="txtId" name="txtId" placeholder="Mã Sản Phẩm" readonly/></th>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <th><input class="form-control" value="{{$product->name}}" id="txtName" name="txtName" placeholder="Tên Sản Phẩm" /></th>
                        </tr>
                        <tr>
                            <th>Category</th>
                            <th><input class="form-control" value="{{$product->id_category}}" type=number id="txtCate" name="txtCate" placeholder="Loại" /></th>
                        </tr>
                        <tr>
                            <th>Price</th>
                            <th><input class="form-control" value="{{$product->price}}" id="txtPrice" name="txtPrice" placeholder="Giá" /></th>
                        </tr>
                        <tr>
                            <th>Des</th>
                            <th><textarea class="form-control" id="txtDes" rows="3" value="{{$product->des}}" id="txtDes" name="txtDes" style="width:537px;height:116px;" >{{$product->des}}</textarea></th>
                        </tr>
                        <tr>
                            <th>Image</th>
                            <th><input class="form-control" value="{{$product->img}}" name="txtImage" id="txtImage" placeholder="Image" required/></th>
                        </tr>
                        
             
                        </table>
                            
                        <button type="submit" class="btn btn-default" value="edit" name="submitbutton" align-center>Edit</button>
                        
                    </div>
                </form>
            </div>
        </div>
    </section>
</section>
<script>
       $('#submit-form').submit(function(e){
        e.preventDefault();
        var txtId = $('#txtId').val();
        var txtName = $('#txtName').val();
        var txtCate = $('#txtCate').val();
        var txtPrice = $('#txtPrice').val();
        var txtDes = $('#txtDes').val();
        var txtImage = $('#txtImage').val();     
        var data = {
            txtId :txtId,
            txtName: txtName,
            txtPrice: txtPrice,
            txtDes: txtDes,
            txtImage: txtImage,
            txtCate: txtCate,
        }
        $.ajax({
            type:'post',
            url:'{!! route('admin/posteditproduct') !!}',
            data: data,
            success:function(data){
                alert(data);
            },
        });
    })
</script>
@endsection