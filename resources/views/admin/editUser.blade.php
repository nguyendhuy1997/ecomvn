@extends('admin.admin')
@section('content')
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-offset-2 col-md-5">
                {{-- @if (count($errors) > 0 )
                    <div class="alert alert-danger">
                        <strong>Xảy ra lỗi!</strong>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                    </div>
                @endif --}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2">
                <form method="post" id="submit-form" action="{!! route('admin/postedituser') !!}"  enctype="multipart/form-data" >
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <table>
                        </tr>
                            <th>Id</th>
                            <th><input class="form-control" value="{{$user->id}}" id='txtId' name="txtId" placeholder="Id" readonly /></th>
                        </tr>
                        </tr>
                            <th>Full Name  </th>
                            <th><input class="form-control" value="{{$user->name}}"  id='txtName' name="txtName" placeholder="Full Name " /></th>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <th><input class="form-control" style="width: 500px" value="{{$user->email}}" id='txtEmail' name="txtEmail" placeholder="Email" /></th>
                        </tr>
                        <tr>
                            <th>Role</th>
                            <th><input class="form-control" style="width: 500px" value="{{$user->role}}" id='txtRole' name="txtRole" placeholder="Role" /></th>
                        </tr>
                        </table>
                            
                        <button type="submit" class="btn btn-default" value="edit" name="submitbutton" align-center>Edit</button>            
                    </div>
                </form>
            </div>
        </div>
    </section>
</section>
<script>
        $('#submit-form').submit(function(e){
         e.preventDefault();
         var txtId = $('#txtId').val();
         var txtName = $('#txtName').val();
         var txtEmail = $('#txtEmail').val();
         var txtRole = $('#txtRole').val();     
         var data = {
             txtId :txtId,
             txtName: txtName,
             txtEmail: txtEmail,
             txtRole: txtRole,
         }
         $.ajax({
             type:'post',
             url:'{!! route('admin/postedituser') !!}',
             data: data,
             success:function(data){
                 alert(data);
             },
         });
     })
 </script>

@endsection