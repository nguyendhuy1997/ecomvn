@extends('admin.admin')
@section('content')
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-offset-2 col-md-5">
                @if (count($errors) > 0 )
                    <div class="alert alert-danger">
                        <strong>Xảy ra lỗi!</strong>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2">
                <form id="insert-form" method="post" action=""  enctype="multipart/form-data" >
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <table>
                        </tr>
                            <th>Full Name  </th>
                            <th><input class="form-control" value="" id="txtName" name="txtFullName" placeholder="Full Name" required="" /></th>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <th><input class="form-control" style="width: 500px" value="" id="txtEmail" name="txtEmail" placeholder="Email" required="" type="email" /></th>
                        </tr>
                        <tr>
                            <th>Password</th>
                            <th><input class="form-control" style="width: 500px" id="txtPass" value="" name="txtPassword" placeholder="Password" required=""/></th>
                        </tr>
                        <tr>
                            <th>Role</th>
                            <th>                    
                                <select id="txtRole">        
                                    <option value=''>User</option>
                                    <option value='admin'>Admin</option>
                                </select>
                            </th>
                        </tr>
                        </table>
                            
                        <button type="submit" class="btn btn-default" value="create" name="submitbutton" align-center>Create</button>
                        
                    </div>
                </form>
            </div>
        </div>
    </section>
</section>
<script>
        $('#insert-form').submit(function(e){
            e.preventDefault();
            var txtName = $('#txtName').val();
            var txtEmail = $('#txtEmail').val();
            var txtPass = $('#txtPass').val();
            var txtRole = $('#txtRole').val();
            var data = {
                txtName: txtName,
                txtEmail: txtEmail,
                txtPass: txtPass,
                txtRole: txtRole,
            }
            $.ajax({
                type:'post',
                url:'{!! route('postadduser') !!}',
                data: data,
                dataType: 'json',
                success:function(data){
                    console.log(data);
                    if(data){
                        alert("Đã thêm thành công.");
                        $('#insert-form')[0].reset();
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    console.log(XMLHttpRequest.responseText); 
                }    
            });
        })
    </script>

@endsection