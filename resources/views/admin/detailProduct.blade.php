@extends('admin.admin')
 @section('content')

<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <table id="table-detail" class="table table-bordered">
                <tr class="success">
                    <th>ID</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>Action</th>
                    <th>Action</th>
                </tr>
                <?php foreach($product as $p){?>
                <tr>
                    <th><?=$p->id_products; ?></th>
                    <th><?=$p->name; ?></th>
                    <th><?=$p->id_category; ?></th>
                    <th><?=number_format($p->price); ?> VNĐ</th>
                    <th><?=$p->des; ?></th>
                    <th><img src="<?=$p->img; ?>" style="width:100px;"></th>
                    <th><a href="{{route('admin/editproduct',$p->id_products)}}">Edit</a></th>
                    <th><a href="{{route('admin/deleteproduct',$p->id_products)}}">Delete</a></th>
                </tr>
            <?php }?>
             </table>
            
        </div>
        <div class="row"></div>
    </section>
</section>

<!--<script>
    $(document).ready(function(){
        $("#table-detail").colResizable({liveDrag:true});
    }
</script>-->
@endsection