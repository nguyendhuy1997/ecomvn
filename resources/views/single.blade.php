@extends('layout.layout')
@section('title', 'This is single page')
@section('content')
<!-- Single Page -->
<div class="banner-bootom-w3-agileits">
    <div class="container">
        <!-- tittle heading -->
        <h3 class="tittle-w3l">Single Page
            <span class="heading-style">
                <i></i>
                <i></i>
                <i></i>
            </span>
        </h3>
        <!-- //tittle heading -->
        <div class="col-md-4 single-right-left ">
            <div class="grid images_3_of_2">
                <div class="flexslider">
                    <ul class="slides">
                        <li data-thumb="images/se1.jpg">
                            <div class="thumb-image">
                                <img src="<?= $product->img;?>" data-imagezoom="true" class="img-responsive" alt="">
                            </div>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4 single-right-left simpleCart_shelfItem">
            <h3>
                <?= $product->name; ?>
            </h3>
            <div class="rating1">
                <span class="starRating">
                    <input id="rating5" type="radio" name="rating" value="5">
                    <label for="rating5">5</label>
                    <input id="rating4" type="radio" name="rating" value="4">
                    <label for="rating4">4</label>
                    <input id="rating3" type="radio" name="rating" value="3" checked="">
                    <label for="rating3">3</label>
                    <input id="rating2" type="radio" name="rating" value="2">
                    <label for="rating2">2</label>
                    <input id="rating1" type="radio" name="rating" value="1">
                    <label for="rating1">1</label>
                </span>
            </div>
            <p>
                <span class="item_price">
                    <?= number_format($product->price) ?>$</span>
                <label>Free delivery</label>
            </p>
            <div class="single-infoagile">
                <ul>
                    <li>
                        <?= $product->des; ?>
                    </li>

                </ul>
            </div>

            <div class="occasion-cart">
                <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                    {{-- <form action="#" method="post">
                        <fieldset>
                            <input type="hidden" name="cmd" value="_cart" />
                            <input type="hidden" name="add" value="1" />
                            <input type="hidden" name="business" value=" " />
                            <input type="hidden" name="item_name" value="Zeeba Premium Basmati Rice - 5 KG" />
                            <input type="hidden" name="amount" value="950.00" />
                            <input type="hidden" name="discount_amount" value="1.00" />
                            <input type="hidden" name="currency_code" value="USD" />
                            <input type="hidden" name="return" value=" " />
                            <input type="hidden" name="cancel_return" value=" " />
                            <input type="submit" name="submit" value="Add to cart" class="button" />
                        </fieldset>
                    </form> --}}
                    <a href="javascript:;"><input type="button" data-id=<?php echo $product->id_products ?> name="submit" value="Add to cart" class="button add-cart" /></a>
                </div>

            </div>

        </div>
        <div class="col-md-4 single-right-left ">
            <div class="lt-table-box technical-info" style="margin-top: 0px;">
                <div class="header">
                    Thông số kỹ thuật
                </div>
                <div class="content">
                    <table>

                        <tbody>
                            <tr itemprop="brand" itemscope="" itemtype="http://schema.org/Brand" style="display: table-row;">
                                <td>Hãng sản xuất:</td>
                                <td itemprop="name">Apple</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>3G:</td>
                                <td>HSPA 42.2/5.76 Mbps, EV-DO Rev.A 3.1 Mbps</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>4G:</td>
                                <td>LTE-A (3CA) Cat9 450/50 Mbps</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Kích thước:</td>
                                <td>158.2 x 77.9 x 7.3 mm (6.23 x 3.07 x 0.29 in)</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Trọng lượng:</td>
                                <td>188 g (6.63 oz)</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Loại SIM:</td>
                                <td>Nano-SIM</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Loại màn hình:</td>
                                <td>Cảm ứng điện dung LED-backlit IPS LCD, 16 triệu màu</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Kích thước màn hình:</td>
                                <td>5.5 inches</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Độ phân giải màn hình:</td>
                                <td>1080 x 1920 pixels</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Hệ điều hành:</td>
                                <td>iOS</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Phiên bản hệ điều hành:</td>
                                <td>11</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Chipset:</td>
                                <td>Apple A10 Fusion APL1W24</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>CPU:</td>
                                <td>2x 2.34 GHz Hurricane &amp; 2x 2.34 GHz Zephyr</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>GPU:</td>
                                <td>PowerVR Series7XT Plus (6 lõi đồ họa)</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Khe cắm thẻ nhớ:</td>
                                <td>Không</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Bộ nhớ đệm / Ram:</td>
                                <td>128 GB, 3 GB RAM</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Camera sau:</td>
                                <td>12 MP (f/1.8, 28mm, 1/3", OIS) + 12 MP (f/2.8, 56mm, 1/3.6"), tự động lấy nét nhận
                                    diện theo giai đoạn, 2x zoom quang học, 4 LED flash (2 tone)</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Camera trước:</td>
                                <td>7 MP (f/2.2, 32mm), 1080p@30fps, 720p@240fps, nhận diện khuôn mặt, HDR</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Quay video:</td>
                                <td>2160p@30fps, 1080p@30/60/120fps, 720p@240fps</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>WLAN:</td>
                                <td>Wi-Fi 802.11 a/b/g/n/ac, dual-band, hotspot</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Bluetooth:</td>
                                <td>4.2, A2DP, LE</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>GPS:</td>
                                <td>A-GPS, GLONASS, GALILEO, QZSS</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>NFC:</td>
                                <td>Yes</td>
                            </tr>



                            <tr style="display: table-row;">
                                <td>USB:</td>
                                <td>2.0</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Cảm biến:</td>
                                <td>Vân tay, gia tốc, la bàn, khoảng cách, con quay quy hồi, phong vũ biểu</td>
                            </tr>


                            <tr style="display: table-row;">
                                <td>Pin:</td>
                                <td>Li-ion 2900 mAh</td>
                            </tr>

                        </tbody>
                    </table>
                    <a id="more-specific" href="#" class="btn btn-default btn-sm" style="display: none;">Xem thêm</a>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>

	<div class="featured-section" id="projects">
		<div class="container">
			<!-- tittle heading -->
			<h3 class="tittle-w3l">Special Offers
				<span class="heading-style">
					<i></i>
					<i></i>
					<i></i>
				</span>
			</h3>
			<!-- //tittle heading -->
			<div class="content-bottom-in">
				<ul id="flexiselDemo1">
				<?php foreach($relativeProduct as $p){ ?>
					<li>
						<div class="w3l-specilamk">
							<div class="speioffer-agile">
                            <a href="{{route('single',['id'=>$p->id_products])}}">
									<img src="<?= $p->img; ?>" width="100" alt="">
								</a>
							</div>
							<div class="product-name-w3l">
								<h4>
									<a href="single/<?= $p->id_products; ?>"><?= $p->name; ?></a>
								</h4>
								<div class="w3l-pricehkj">
									<h6><?= number_format($p->price)  ?> ₫</h6>
								</div>
								<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                        <a href="javascript:;"><input type="button" data-id=<?php echo $p->id_products ?> name="submit" value="Add to cart" class="button add-cart" /></a>
								</div>
							</div>
						</div>
					</li>
				<?php }?>
				</ul>

			</div>
		</div>
	</div>
	<!-- //special offers -->

<!-- //Single Page -->
@endsection