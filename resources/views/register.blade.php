<!--Author: W3layouts
	 
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html lang="zxx">

<head>
	<title>Classic Register Form Responsive Widget Template :: w3layouts</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<meta name="keywords" content="Classic Register Form Responsive Widget,Login form widgets, Sign up Web forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements"
	/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<script>
			$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
	</script>
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- Meta tag Keywords -->
	<!-- css files -->
	<link rel="stylesheet" href="./css/register.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link rel="stylesheet" href="./css/register-font.css">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->
	<!-- web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
	<!-- //web-fonts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
	<!--header-->
	<h1>
	
		<span>Register</span>
		</h1>
	<!--//header-->
	<!-- content -->
	<div class="main-content-agile">
		<div class="sub-main-w3">
            <form action="{{route('Register')}}" method="post" >
                @csrf
				<div class="form-style-agile">
					<label>Your Name</label>
					<div class="pom-agile">
						<input placeholder="Your Name" name="txtName" id="name" type="text" required="">
						<span class="fa fa-user-o" aria-hidden="true"></span>
					</div>
				</div>
				<div class="form-style-agile">
					<label>Email</label>
					<div class="pom-agile">
						<input placeholder="Email" id="email" name="txtEmail" type="email" required=""  >
						<span class="fa fa-envelope" aria-hidden="true"></span>
					</div>
					<div class="status" style="color:red"></div>
				</div>
				<div class="form-style-agile">
					<label>Password</label>
					<div class="pom-agile">
						<input placeholder="Password" name="txtPassword" id="pass" type="password"  required="" onkeyup='check();'>
						<span class="fa fa-key" aria-hidden="true"></span>
					</div>
				</div>
				<div class="form-style-agile">
					<label>Confirm Password</label>
					<div class="pom-agile">
						<input placeholder="Confirm Password" name="Confirm Password" type="password" id="password2" required="" onChange="checkPasswordMatch();" >
						<span class="fa fa-key" aria-hidden="true"></span>
					</div>
					<div id='message' style="color:red"></div>
				</div>
				
				<div class="clear"></div>
				
			
				<input id="submit" type="submit" value="Register">
		
			</form>
			
			@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
	@endif
		</div>
    </div>



	<!-- //content -->
	<!--footer-->
	<div class="footer">
		<h2>&copy; <p style="font-size:20px">&copy; 
							<a size="6" href="{{route('homepage')}}">Homepage</a>
					</p>
		</h2>
	</div>
	<!--//footer-->


	<!-- password-script -->
	<!-- <script>
		window.onload = function () {
			document.getElementById("password1").onchange = validatePassword;
			document.getElementById("password2").onchange = validatePassword;
		}

		function validatePassword() {
			var pass2 = document.getElementById("password2").value;
			var pass1 = document.getElementById("password1").value;
			if (pass1 != pass2)
				document.getElementById("password2").setCustomValidity("Passwords Don't Match");
			else
				document.getElementById("password2").setCustomValidity('');
			//empty string means no validation error
		}
	</script> --> 
	<!-- //password-script-->




</body>
<script>
function checkPasswordMatch() {
    var password = $("#pass").val();
    var confirmPassword = $("#password2").val();
    if (password != confirmPassword)
	{
		$("#message").html("Passwords do not match!");
		$('#submit').attr({'disabled':''});
	}
    else{
		$("#message").html("");
		$('#submit').removeAttr('disabled');
	}
        
}
</script>
<script type="text/javascript">

	$('#email').blur(function(){
				var email = $("#email").val();
				$.ajax({
				type:'get',
				url:'{{route('checkEmail')}}',
				data:{email:email},
				success:function(data){
					if(data=='false')
					{
						$('.status').html("email is available");
						$('#submit').attr({
							'disabled':''
						});
					}
					else{
						$('.status').html("");
						$('#submit').removeAttr('disabled');
					}
				}
				});
	});
</script>

</html>