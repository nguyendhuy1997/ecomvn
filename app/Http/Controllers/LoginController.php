<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Users;
use App\Products;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Hash;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('login');
    }
    public function postLogin(Request $request)
    {

        if (Auth::attempt(['email' => $request->txtEmail, 'password' => $request->txtPassword])) {
            return redirect()->intended('/');
        } else {
            return redirect()->back()->with('message', 'Tài khoản chưa được đăng ký');
        }

    }
    public function logOut()
    {
        Auth::logout();
        return redirect()->back();
    }
}
