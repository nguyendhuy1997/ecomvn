<?php

namespace App\Http\Controllers;
use Hash;
use App\Users;
use App\User;
use App\Products;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Category;

class AdminController extends Controller
{
    public function adminPage()
    {
        return view('admin.index');
    }
    public function addProduct()
    {
        $categories = Category::get();   
        return view('admin.addProduct',compact('categories'));
    }
    public function postAddProduct(Request $request)
    {
        $product = new Products();
        $product->id_category=$request->txtCategory;
        $product->name=$request->txtName;
        $product->quantity=$request->txtQuantity;
        $product->price=$request->txtPrice;
        $product->des=$request->txtDesc;
        $product->img=$request->txtImage;
        $rs = $product->save();
        if($rs == 1) return "true";
        return 0;
    }
    public function detailProduct()
    {
        $product = DB::table('products')->get();
        return view('admin.detailProduct',compact('product'));
    }
    public function editProduct($id)
    {
        $product=Products::where('id_products',$id)->first();
        return view('admin.editProduct',compact('product'));
    }
    public function postEditProduct(Request $request)
    {
       
       try{
            Products::where('id_products',$request->txtId)->update(array(
                'price'=>$request->txtPrice,
                'name'=>$request->txtName,
                'id_category'=>$request->txtCate,
                'des'=>$request->txtDes,
                'img'=>$request->txtImage,
            ));
            return 'Đã sửa thành công';
        }
        catch (\Exception $e) {
            return 'Có lỗi xảy ra';
        }
    }
    public function deleteProduct($id)
    {
        $product=Products::where('id_products',$id)->delete();
        return redirect()->route('detailproduct');   
    }
    public function addUser()
    {
        return view('admin.addUser');
    }
    public function postAddUser(Request $request)
    {
        $user = new Users();
        $user->name=$request->txtName;
        $user->email=$request->txtEmail;
        $user->password=Hash::make($request->txtPass);
        $user->role=$request->txtRole;
        $rs = $user->save();
        if($rs == 1) return "true";
        return 0;
    }
    public function editUser($id)
    {
        $user=User::where('id',$id)->first();
        return view('admin/edituser',compact('user'));
    }
    public function postEditUser(Request $request)
    {
        try{
            Users::where('id',$request->txtId)->update(array(
                'name'=>$request->txtName,
                'email'=>$request->txtEmail,
                'role'=>$request->txtRole,
            ));
            return 'Đã sửa thành công';
        }
        catch (\Exception $e) {
            return 'Có lỗi xảy ra';
        }
    }
    public function deleteUser($id)
    {
        $user=users::where('id',$id)->delete();
        return redirect()->route('admin/listuser');   
    }
    public function listUser()
    {
        $user = User::get();   
        return view('admin.listUser',compact('user'));
    }
}
