<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;

class SingleController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }
    public function getSingle($id)
    {
       $product=Products::where('id_products',$id)->first();
       $relativeProduct=Products::where('id_category',$product->id_category)->raw('RAND()')->take(5)->get();
       return view('single',compact('product','relativeProduct'));
    }
}
