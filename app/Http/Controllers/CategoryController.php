<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;

class CategoryController extends Controller
{
    public function getCategory($id)
    {
       $product=Products::where('id_category','=',$id)->orderby('id_products','DESC')->limit(3)->offset(0)->get();
    //    $relativeProduct=Products::where('id_category',$product->id_category)->raw('RAND()')->take(5)->get();
       return view('category',compact('product'));
    }
    public function loadMore(Request $request)
    {
        $offSet=$request->offSet;
        $id=$request->id;
        $product=Products::where('id_category','=',$id)->orderby('id_products','DESC')->limit(3)->offset($offSet)->get();
        return $product;
    }
}
