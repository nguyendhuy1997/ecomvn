<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use Hash;
use Symfony\Component\HttpFoundation\Response;

class RegisterController extends Controller
{
    //
    public function getRegister()
    {
        return view('register');
    }
    public function postRegister(Request $request)
    {
            $user = new Users();
            $user->name=$request->txtName;
            $user->email=$request->txtEmail;
            $user->password=Hash::make($request->txtPassword);
            $user->save();
            return redirect()->route('login');

    }
    public function checkEmail(Request $request)
    {
        $email = Users::where('email',$request->email)->value('email');
        if($email==NULL)
        {
            echo "true";
        }
        else
        {
           echo "false";
        }
    }   
}
