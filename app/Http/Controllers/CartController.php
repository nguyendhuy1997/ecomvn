<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Bill;
use App\Billdetail;
use App\Products;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

session_start();

class CartController extends Controller
{
    public function addCart(Request $request)
    {
        $result = Products::where('id_products', $request->id_products)->first();
        if (empty($_SESSION['cart'][$result->id_products])) {
            $_SESSION['cart'][$result->id_products] = array(
                "id" => $result->id_products,
                "name" => $result->name,
                "img" => $result->img,
                "price" => $result->price,
                "quantity" => 1
            );
        } else {
            if (array_key_exists($result->id_products, $_SESSION['cart'])) {
                $_SESSION['cart'][$result->id_products] = array(
                    "id" => $result->id_products,
                    "name" => $result->name,
                    "img" => $result->img,
                    "price" => $result->price,
                    "quantity" => $_SESSION['cart'][$result->id_products]['quantity'] + 1
                );
            }
        }
        return count($_SESSION['cart']);
    }
    public function loadCart()
    {
        return view('cart');
    }
    public function updateCart(Request $request)
    {
        $result = Products::where('id_products', $request->id_products)->first();
        if (empty($_SESSION['cart'][$result->id_products])) {
            return false;
        } else {
            if (array_key_exists($result->id_products, $_SESSION['cart'])) {
                if ($request->type == "minus") {
                    if ($_SESSION['cart'][$result->id_products]['quantity'] == 1) {
                        $_SESSION['cart'][$result->id_products] = array(
                            "id" => $result->id_products,
                            "name" => $result->name,
                            "img" => $result->img,
                            "price" => $result->price,
                            "quantity" => 1
                        );
                        return 0;
                    } else {
                        $_SESSION['cart'][$result->id_products] = array(
                            "id" => $result->id_products,
                            "name" => $result->name,
                            "img" => $result->img,
                            "price" => $result->price,
                            "quantity" => $_SESSION['cart'][$result->id_products]['quantity'] - 1
                        );
                    }

                } else {
                    $_SESSION['cart'][$result->id_products] = array(
                        "id" => $result->id_products,
                        "name" => $result->name,
                        "img" => $result->img,
                        "price" => $result->price,
                        "quantity" => $_SESSION['cart'][$result->id_products]['quantity'] + 1
                    );
                }

            }
        }
        return $_SESSION['cart'][$result->id_products]['quantity'];
    }
    public function deleteCart(Request $request)
    {
        $result = Products::where('id_products', $request->id_products)->first();
        if (array_key_exists($result->id_products, $_SESSION['cart'])) {
            unset($_SESSION['cart'][$request->id_products]);
            return 'true';
        }
        else return 'false';
    }
    public function submitBill(Request $request)
    {
        $total=0;
        foreach ($_SESSION['cart'] as $p)
        {
            $total+=$p['price']*$p['quantity'];
        }
        $mytime = Carbon::now();
        $bill = new Bill();
        $id=Bill::count();
        $bill->id_bill=$id+1;
        $bill->total=$total;
        $bill->id_user=Auth::user()->id;
        $bill->date=$mytime;
        $bill->phone=$request->phone;
        $bill->address=$request->address;
        $bill->name=$request->name;
        $bill->save();
        foreach($_SESSION['cart'] as $p)
        {
            $bill_detail = new Billdetail();
            $bill_detail->id_bill=$bill->id_bill;
            $bill_detail->id_products=$p['id'];
            $bill_detail->quantity=$p['quantity'];
            $bill_detail->save();
        }

        return redirect()->route('homepage');

    }
}
