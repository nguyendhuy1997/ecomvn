<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
   public function getProduct(){
    $product = DB::table('products')->paginate(6);
    $tivi = Products::where('id_category','3')->get();
       return view('home', compact('product','tivi'));
   }
   public function getSearch(Request $request){
    $product=Products::Where('name', 'like', '%' . $request->txtSearch . '%')->get();
    return view('search',compact('product'));
   }
}
