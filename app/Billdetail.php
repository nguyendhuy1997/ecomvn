<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billdetail extends Model
{
    protected $table = 'billdetail';
    public $incrementing = false;
    public $timestamps = false;
}
