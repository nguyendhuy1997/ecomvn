<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', [
    'as' => 'homepage',
    'uses' => 'HomeController@getProduct'
]);
//search
Route::get('/search',[
	'as'=>'search',
	'uses'=>'HomeController@getSearch'
]);
// Route::post(function(){
//     return "POST homepage";
// });
Route::get('single/{id}', [
    'as' => 'single',
    'uses' => 'SingleController@getSingle'
]);

Route::get('/register',[
	'as'=>'register',
	'uses'=>'RegisterController@getRegister'
]);
Route::post('/register',[
	'as'=>'Register',
	'uses'=>'RegisterController@postRegister'
]);
Route::get('/checkEmail',[
	'as'=>'checkEmail',
	'uses'=>'RegisterController@checkEmail'
]);
Route::get('/login',[
	'as'=>'login',
	'uses'=>'LoginController@getLogin'
]);
Route::post('/login',[
	'as'=>'postLogin',
	'uses'=>'LoginController@postLogin'
]);
Route::get('/logout',[
	'as'=>'logout',
	'uses'=>'LoginController@logOut'
]);
Route::get('/category/{id}', [
    'as' => 'category',
    'uses' => 'CategoryController@getCategory'
]);
Route::get('/loadmore', [
	'as'=>'loadmore',
	'uses'=>'CategoryController@loadMore']);
Route::get('/addcart', [
    'as' => 'addcart',
    'uses' => 'CartController@addCart'
]);
Route::get('/updatecart', [
    'as' => 'updatecart',
    'uses' => 'CartController@updateCart'
]);
Route::get('/deletecart', [
    'as' => 'deletecart',
    'uses' => 'CartController@deleteCart'
]);
Route::get('/cart', [
	'as'=>'cart',
	'uses'=>'CartController@loadCart'
	])->middleware('checkAuth');
Route::post('/bill', [
    'as' => 'bill',
    'uses' => 'CartController@submitBill'
]);
//<--Admin Route-->

route::group(['prefix'=>'admin','middleware'=>'checkAdmin'],function(){
	Route::get('/', [
		'as' => 'admin',
		'uses' => 'AdminController@adminPage'
	]);
	Route::get('/addproduct', [
		'as' => 'addproduct',
		'uses' => 'AdminController@addProduct'
	]);
	Route::post('/addproduct', [
		'as' => 'addproduct',
		'uses' => 'AdminController@postAddProduct'
	]);
	Route::get('/adduser', [
		'as' => 'adduser',
		'uses' => 'AdminController@addUser'
	]);
	Route::post('/adduser', [
		'as' => 'postadduser',
		'uses' => 'AdminController@postAddUser'
	]);
	Route::get('/detailproduct', [
		'as' => 'detailproduct',
		'uses' => 'AdminController@detailProduct'
	]);
	Route::get('editproduct/{id}', [
		'as' => 'admin/editproduct',
		'uses' => 'AdminController@editProduct'
	]);
	Route::post('editproduct', [
		'as' => 'admin/posteditproduct',
		'uses' => 'AdminController@postEditProduct'
	]);
	Route::get('deleteproduct/{id}', [
		'as' => 'admin/deleteproduct',
		'uses' => 'AdminController@deleteProduct'
	]);
	Route::get('deleteuser/{id}', [
		'as' => 'admin/deleteuser',
		'uses' => 'AdminController@deleteUser'
	]);
	Route::get('/edituser/{id}', [
		'as' => 'admin/edituser',
		'uses' => 'AdminController@editUser'
	]);
	Route::post('edituser', [
		'as' => 'admin/postedituser',
		'uses' => 'AdminController@postEditUser'
	]);
	Route::get('/listuser', [
		'as' => 'admin/listuser',
		'uses' => 'AdminController@listUser'
	]);
});
//<--/Admin Route-->


// Route::post('/checkEmail','RegisterController@checkEmail');
